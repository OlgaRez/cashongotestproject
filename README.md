# 'Cash On Go' Promo Code Test Project

## Prerequisites
This project has dependencies that require Node together
with NPM and Git

### Instruction for ubuntu desktop
#### install nodejs and npm, if you don't have it: 

```bash
$ sudo apt-get install nodejs
$ sudo apt-get install npm
```
#### install Git, if you don't have it: 

```bash
$ sudo apt-get install git
```

## Installation

### Clone repository with tests

```bash
git clone https://bitbucket.org/OlgaRez/cashongotestproject.git
```
 
### Navigate to project folder

```bash
$ cd cashongotestproject
```

### Install

```bash
npm install
```

## Usage

### Open Cypress with tests
```bash
npm run open
```
### Run implemented tests (Progress is visible in browser)
```bash
npm run dev
```

### Run implemented tests in headless mode (no browser appear)
```bash
npm run test
```
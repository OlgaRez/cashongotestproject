/// <reference types="Cypress" />

var testData = [
  {
    amount: 100,
    payments: 1,
  },
  {
    amount: 100,
    payments: 2
  },
  {
    amount: 100,
    payments: 6
  },
  {
    amount: 100,
    payments: 12
  },
  {
    amount: 500,
    payments: 2
  },
  {
    amount: 500,
    payments: 6
  },
  {
    amount: 500,
    payments: 12
  },
  {
    amount: 1000,
    payments: 2
  },
  {
    amount: 1000,
    payments: 6
  },
  {
    amount: 1000,
    payments: 12
  }
];
var creditAmount = '[id="AmountOfCreditVal"]';
var paymentAmount = '[id="LoanPaymentsVal"]';
var promoCode = '[id="promocode"]';
var promoCodeBtn = '[id="promocode-button"]';
var discountedTotal = '[id="calc_new_total_payable"]';
var totalToPay = '[id="calc_total_payable"]';

describe('Tests enetring promo code manually', function () {
  it('Go to "www.peachy.co.uk"', function () {
    cy.server();
    cy.route('GET', '/calculator/config').as('getConfig');

    cy.visit('www.peachy.co.uk');

    cy.wait('@getConfig')
  })
  it('Add promo code', () => {
    cy.get(promoCode).type('5OFF');
    cy.get(promoCodeBtn).click();
    cy.wait(1500);
  })
  testData.forEach((data) => {

    it('Check ' + data.amount + ' amount for ' + data.payments + ' months', function () {

      cy.get(creditAmount).invoke('val', data.amount).trigger('change', { force: true });
      cy.wait(1000);
      cy.get(creditAmount).should('have.value', data.amount.toString());


      cy.get(paymentAmount).invoke('val', data.payments).trigger('change', { force: true });
      cy.wait(1000);
      cy.get(paymentAmount).should('have.value', data.payments.toString());
      cy.get('[class="value-wrap payments"]').should('have.text', data.payments.toString());
      cy.get(promoCodeBtn).click();
      cy.wait(1000);

      if (data.payments == 1) {
        cy.get('[id="promo-code-info"]').should('have.text', 'Promotion is valid only for loans with at least 2 payments');
      } else {
        cy.get(totalToPay).should('have.css', 'text-decoration', 'line-through solid rgb(122, 114, 127)');
        cy.get(totalToPay).then(input => {
          var totalToPayValue = input.text().substring(1);
          var discounted = '£' + (totalToPayValue - 5);
          cy.get(discountedTotal).should('be.visible');
          cy.get(discountedTotal).should('have.text', discounted);
        });


      }
      //£
    })
  })
})
/*describe('Test smallest credit amount', function() {
   

  it('Go to "www.peachy.co.uk"', function() {
    cy.server();
    cy.route('GET','/calculator/config').as('getConfig');

    cy.visit('www.peachy.co.uk');
    
    cy.wait('@getConfig')
  })

  it('Check amount and payments default values',function() {

    creditValue = '£400';
    cy.get(creditAmount).should('have.value', '400');
    cy.get('[class="value-wrap amount"]').should('have.text', creditValue);      

    paymentValue = '6';
    cy.get(paymentAmount).should('have.value', paymentValue);
    cy.get('[class="value-wrap payments"]').should('have.text', paymentValue);

  })

  it('Check Default Loan Details',function(){

    creditValue = '£400.00';
    avgPerPayValue = '£121.82';
    totalToPayValue = '£730.93';
    CheckCreditDetails();

  })

  it('Change credit amount to minimal and check Loan Details',function() {
      
    cy.get(creditAmount).invoke('val', 100).trigger('change', {force: true});

    cy.get(creditAmount).should('have.value', '100');

    creditValue = '£100'
    cy.get('[class="value-wrap amount"]').should('have.text', creditValue);
    creditValue = '£100.00';
    avgPerPayValue = '£31.36';
    totalToPayValue = '£188.18';
    CheckCreditDetails();
      

      //console.log('x',cy.get(paymentAmount))   

      //cy.get(paymentAmount).then(input=>{
      //  const val = Number(input.val());
      //});
  })


  it('Change payments amount to 1 and check Loan details', function(){
    paymentValue = '1';

    cy.get(paymentAmount).invoke('val', paymentValue).trigger('change', {force: true});

    cy.get(paymentAmount).should('have.value', paymentValue);
    cy.get('[class="value-wrap payments"]').should('have.text', paymentValue);
      
    avgPerPayValue = '£124.00';
    totalToPayValue = '£124.00';
    CheckCreditDetails();
     
  })

  it('Change payments amount to 12 and check Loan details',function(){
    paymentValue = '12';
      
    cy.get(paymentAmount).invoke('val', paymentValue).trigger('change', {force: true});

    cy.get(paymentAmount).should('have.value', paymentValue);
    cy.get('[class="value-wrap payments"]').should('have.text', paymentValue);
      
    creditValue = '£100.00';
    avgPerPayValue = '£16.25';
    totalToPayValue = '£195.03';
    CheckCreditDetails();
  })

  it('Add discount code "5OFF" and check Loan details',function(){
    cy.get(promoCode).type('5OFF');      
    cy.get(promoCodeBtn).click();
    cy.wait(1000);

    creditValue = '£100.00';
    avgPerPayValue = '£15.84';
    totalToPayValue = '£195.03';
    discountedValue = '£190.03'
    CheckCreditDetails();
    cy.get(totalToPay).should('have.css', 'text-decoration', 'line-through solid rgb(122, 114, 127)');    
    cy.get(discountedTotal).should('be.visible').and('have.text', discountedValue);    

  })

  it('Change payments to 1 and check Loan details',function(){

    paymentValue = '1';
    
    cy.get(paymentAmount).invoke('val', paymentValue).trigger('change', {force: true});
    cy.get(promoCodeBtn).click();
    cy.wait(1000);

    avgPerPayValue = '£124.00';
    totalToPayValue = '£124.00';
    cy.get(discountedTotal).should('be.hidden')
    CheckCreditDetails();
    cy.get('[id="promo-code-info"]').should('have.text', 'Promotion is valid only for loans with at least 2 payments');

  })

  it('Change payments to 2 and check Loan details', function(){
    paymentValue = '2';
    cy.get(paymentAmount).invoke('val', paymentValue).trigger('change', {force: true});    
    cy.get(promoCodeBtn).click();
    cy.wait(1000);

    avgPerPayValue = '£66.46';
    totalToPayValue = '£137.91';
    CheckCreditDetails();
    cy.get(totalToPay).should('have.css', 'text-decoration', 'line-through solid rgb(122, 114, 127)');
    cy.get(discountedTotal).should('be.visible').and('have.text', '£132.91');

  })

  it('Change payments to 6 and check Loan details',function(){
    paymentValue = '6';
    cy.get(paymentAmount).invoke('val', paymentValue).trigger('change', {force: true});
    cy.get(promoCodeBtn).click();
    cy.wait(1000);

    avgPerPayValue = '£30.53';
    totalToPayValue = '£188.18';
    CheckCreditDetails();
    cy.get(totalToPay).should('have.css', 'text-decoration', 'line-through solid rgb(122, 114, 127)');
    cy.get(discountedTotal).should('be.visible').and('have.text', '£183.18');
  })     */

//})


function CheckCreditDetails() {
  cy.get('[id="amount_of_credit"]').should('have.text', creditValue);
  cy.get('[id="no_of_payments"]').should('have.text', paymentValue);
  cy.get('[id="calc_average_payment"]').should('have.text', avgPerPayValue);
  cy.get('[id="calc_total_payable"]').should('have.text', totalToPayValue);
}

function MakeCalculations() {

}